#Playbook's README

Projekt i implementacja systemu rezerwacji biletów dla teatrów - praca inżynierska Szymona Zawadzkiego.

System Playbook 

W skład systemu wchodzi:

0. Baza Danych na platformie PostgreSQL (schemat dostepny w folderze db w plikach Panelu Administracyjnego);
1. Panel Administracyjny - PlayBook Panel;
2. Aplikacja Webowa - PlayBook Web;
